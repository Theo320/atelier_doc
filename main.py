"""
main.py
===============================================
Ce fichier contient des fonctions qui permettent de tester la documentation
"""

def helloworld() -> str:
    """
    Cette fonction permet de retourner Hello world en string
    :returns: Hello World
    :rtype: str
    """
    return "Hello world"

def soustraction(a:int, b:int) -> int:
    """
    Cette fonction permet de soustraire 2 nombres et de retourner le résultat.
    :func:'~main.main'

    :param a: Le premier nombre
    :type a: int
    :param b: Le deuxième nombre
    :type b: int
    :returns: La soustraction des deux nombres
    :rtype: int
    """
    return a-b

def addition(a:int, b:int) -> int:
    """
    Cette fonction permet d'additioner 2 nombres et de retourner le résultat.
    :func:'~main.main'

    :param a: Le premier nombre
    :type a: int
    :param b: Le deuxième nombre
    :type b: int
    :returns: La sommes des deux nombres
    :rtype: int
    """
    return a+b


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
