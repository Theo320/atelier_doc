.. atelier_doc documentation master file, created by
   sphinx-quickstart on Tue Feb  9 15:58:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans l'atelier de documentation logicielle !
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   referentiel
   faq
   guide-utilisateur
   tutoriel



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Readme File (README.rst)
-------------------------

.. include:: ../../README.rst
